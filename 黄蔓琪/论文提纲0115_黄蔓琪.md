### 一、研究目的
国际上对目标资本结构的调整速度（ SOA ）通常采用部分调整模型进行估计，其中账面负债率和市值负债率的 SOA 通常作为相异不大的两种被解释变量被共同报告出来，估计方式未做区分。但有多篇文献的实证证据表明，公司朝着目标账面负债率做调整的积极性要明显高于市值负债率（如Kisgen（2009），Welch（2004），Graham and Harvey（2001）等等）。近年来学者开始对 SOA 进行分解，以区分出被动的调整速度和企业主动的目标调整速度。Faulkender et al. (2012)对账面负债率做了净利润调整，分解出主动的账面 SOA ，其估计方法已被广泛应用到对中国企业的研究中（黄继承，阚铄，朱冰等（2016）；巫岑，黎文飞和唐清泉（2019）等）；Yin and Ritter（2019）通过控制公司市值增长率的波动性，分解出主动的市值 SOA ，并估计得出1965-2013年间美国企业的主动市值 SOA 为10.5%，与修正前的25.7%存在明显偏差，反映出权衡理论的解释力偏弱。但该方法在中国市场尚未得到应用和检证。  

针对中国企业，大多数动态资本结构的研究采用账面 SOA 指标，结果各异：黄继承，阚铄，朱冰等（2016）控制年度效应后估计中国企业主动账面 SOA 为30.2%（2001~2012年）；W. Li et al.（2017）估计结果为8.8%（2004-2012年），巫岑，黎文飞和唐清泉（2019）采用GMM方法估计结果为9%（2006-2015年）。Mai, Meng and Ye（2017）提供了少有的对中国市值 SOA 的估计，控制宏观经济因素后2000-2014年间估计值为16.7%，且发现按地区划分，东部＞中部＞西部，皆低于美国修正前水平。若按Yin and Ritter（2019）的结论，我国实际市值 SOA 应处于更低水平，故本文将运用中国企业数据对其 SOA 分解模型进行验证，以进一步探究中国企业的资本结构决策模式。

### 二、主要内容  
**核心问题：**  
“公司市值波动致使市值负债率的目标调整速度上偏”现象在中国市场的实证检验  
**子问题：**
1. 中国上市公司的市值负债率及其目标调整速度（ SOA ）在传统的部分调整模型和Yin and Ritter（2019）的 SOA 分解模型修正下的估计值分别是多少？是否存在较大偏差？
2. 经修正的中国市值 SOA 数值，及其与未修正前的偏差值，与Yin and Ritter（2019）中对美国企业的估计结果相比是否存在明显差异？差异的原因？（能否与股票市场的波动性对应？）
3. 对不同特质的公司（eg. 国企 VS 民企、under-levered VS over-levered等），其修正的市值 SOA 和偏差程度是否存在差异？
4. 实证结果反映出权衡理论、市场择时、啄食理论对中国公司资本结构决策何者更为适用？

### 三、可行性分析
#### 数据和变量：  
数据来自国泰安数据库的沪、深证券市场1998~2018年间的全部 A 股上市公司数据，剔除金融和保险企业  
**核心变量：**  
账面负债率=账面负债/账面总资产  
市值负债率=账面负债/（账面负债+权益市值）  
**其他控制变量：**  
总资产、托宾Q、EBIT、净固定资产、研发费用、评级、行业杠杆率中值等  
（同时考虑是否加入年度变量（Chang et al. ( 2014) ，Flannery and Hankins( 2013)）、宏观经济变量（Mai, Meng and Ye，2017））
#### 模型和方法：  
**（一）负债率 SOA 研究的常用方法：**  
1. 含固定效应的部分调整模型+不同的估计方法：  
* 标准的部分调整模型来自Flannery and Rangan（2006）：  
    * `Flannery, M., and K. Rangan. “Partial Adjustment toward Target Capital Structures.” Journal of Financial Economics, 79 (2006), 469-506.`  
$$
LEV_{i,t}-LEV_{i,t-1}=\lambda(LEV^*_{i,t}-LEV_{i,t-1})+\epsilon_{i,t}
$$
$$
LEV^*_{i,t}=\beta X_{i,t-1}
$$
考虑公司固定效应：
$$
LEV_{i,t}=\lambda \beta X_{i,t-1}+(1-\lambda)LEV_{i,t-1}+c_i+\epsilon_{i,t}
$$
* Faulkender, Flannery, Hankins, Smith(2012)做了拓展：考虑现金流影响，对账面总资产做了净利润的调整：
    * `Faulkender, M., M. J. Flannery, K. Watson Hankins, J. M. Smith, 2012, Cash flows and leverage adjustments, Journal of Financial Economics, 103 (3): 632-646.`  
$$
LEV_{i,t}-LEV^p_{i,t-1}=\lambda(LEV^*_{i,t}-LEV^p_{i,t-1})+\epsilon_{i,t}
$$
其中，$LEV^p_{i,t-1}=D_{i,t-1}/(A_{i,t-1}+NI_{i,t})$，NI为净利润  
* 估计方法：
    * GMM（Faulkender et al. (2012) 、Li et al. (2017)）
    * IV估计（Flannery and Rangan, 2006）
    * 长差分估计（Huang and Ritter(2009), Hahn, Hausman, and Kuersteiner(2007)）
    * 最小二乘虚拟变量法模型( LSDVC) (黄继承和姜付秀, 2015)  

2. DPT模型
* `Elsas, R., and D. Florysiak. “Dynamic Capital Structure Adjustment and the Impact of Fractional
Dependent Variables.” Journal of Financial and Quantitative Analysis, 50 (2015), 1105-1133`  
$$
y^*_{i,t}=z_{i,t}\gamma+y_{i,t-1}(1-\lambda)+c_i+u_{i,t}
$$
$$
y_{i,t}=
\begin{cases}
0, if y^*_{i,t}\leq 0 \\
y^*_{i,t}, if 0<y^*_{i,t}<1 \\
1, if y^*_{i,t}\geq 0 
\end{cases}
$$
$$
c_i=\alpha_0+\alpha_1 y_{i,0}+E(z_i)\alpha_2+a_i
$$
其中$y^*_{i,t}$是杠杆率的潜变量，$c_i$代表公司固定效应  

3.  SOA 分解模型
* `Yin Q E , Ritter J R . The Speed of Adjustment to the Target Market Value Leverage Is Slower Than You Think[J]. Journal of Financial and Quantitative Analysis,(2019)`
$$
LEV_{i,t}-LEV_{i,t-1}=\lambda(LEV^*_{i,t}-LEV_{i,t-1})+\epsilon_{i,t}
$$
$$
LEV_{i,t}=D_{i,t}/A_{i,t-1}=(1-\frac{g_{i,t}}{1+g_{i,t}})LEV_{i,t-1}+\frac{g_{i,t}}{1+g_{i,t}
}\times\frac{d_{i,t}}{g_{i,t}}
$$
$$
\begin{cases}
\frac{d_{i,t}}{g_{i,t}}=w_1+w_2N^-_{i,t}+\beta_1 LEV_{i,t-1}+\beta_2 LEV_{i,t-1}N^-_{i,t}+w_{i,t}\\
\frac{g_{i,t}}{1+g_{i,t}}=z+z_2N^-_{i,t}+\delta_1 LEV_{i,t-1}+\delta_2 LEV_{i,t-1}N^-_{i,t}+z_{i,t}
\end{cases}
$$
其中，$d_{i,t}=\Delta D_{i,t}/A_{i,t-1};g_{i,t}=\Delta A_{i,t}/A_{i,t-1}; N^-_{i,t}=I(g_{i,t}<0)$，虚拟变量的作用在于考虑同一公司市值增长率的正负波动。联立以上四式，得到SOA的表达式：
$$
\lambda=z_1(1-\beta_1)-w_1\delta_1+\delta_1(1-\beta_1)f(LEV_{i,t-1})+(\delta_2(1-\beta_1)-\delta_1+\delta_2)\beta_2)\frac{Cov(LEV^2_{i,t-1}N^-_{i,t},LEV_{i,t-1})}{\sigma^2_L}+(z_2(1-\beta_1-(z_1+z_2)\beta_2-w_1\delta_2-(\delta_1+\delta_2)w_2)\frac{Cov(LEV_{i,t-1}N^-_{i,t},LEV_{i,t-1})}{\sigma^2_L}-(z_1w_2+z_2w_1+z_2w_2)\frac{Cov(N^-_{i,t},LEV_{i,t-1})}{\sigma^2_L}
$$
其中，$f(LEV_{i,t-1})=[E(LEV^3_{i,t-1})-E(LEV^2_{i,t-1})E(LEV_{i,t-1})]/\sigma^2_L $  
根据DPT模型，对公司固定效应进行调整：
$$
\hat{c_i}=\hat{\alpha_0}+\hat{\alpha_1 }LEV_{i,0}+E(z_i)\hat{\alpha_2}
$$
假设账面资产增长率$g^B_{i,t}$和市值资产增长率$g^M_{i,t}$的关系为：
$$
g^M_{i,t}=m(g^B_{i,t}+\tau_{i,t}),\tau_{i,t}\sim N(0,\eta)
$$
为去除市值增长率波动的影响，令$g^M_{i,t}=g^B_{i,t}, c^M=c^B$，则得到调整了公司固定效应且修正后的主动市值SOA表达式：
$$
\lambda'=\frac{1}{2}(\lambda+\sqrt{\lambda^2+4\frac{\sigma^2_c}{\sigma^2_L}})
$$ 
**（二）本文拟采用方法：**
1. 依照Yin and Ritter（2019），采用考虑公司市值增长率正负波动的 SOA 分解模型，结合DPT模型中的公司固定效应，关注β、修正前的账面 SOA 、市值 SOA 及修正后的市值 SOA 
2. 采用含固定效应的部分调整模型进行估计，与1的结果比较
3. 在1中的 SOA 分解模型的回归模型中控制滞后一期的公司特征、加入高阶项，做稳健性检验：
$$
\begin{cases}
\frac{d_{i,t}}{g_{i,t}}=w_1+w_2N^-_{i,t}+(\beta_1+f_1x) LEV_{i,t-1}+\beta_2 LEV_{i,t-1}N^-_{i,t}+f_2X_{i,t-1}+w_{i,t}\\
\frac{g_{i,t}}{1+g_{i,t}}=z+z_2N^-_{i,t}+(\delta_1+h_1x) LEV_{i,t-1}+\delta_2 LEV_{i,t-1}N^-_{i,t}+h_2X_{i,t-1}+z_{i,t}
\end{cases}
$$
$$
\begin{cases}
\frac{d_{i,t}}{g_{i,t}}=w_1+w_2N^-_{i,t}+\beta_1 LEV_{i,t-1}+\beta_2 LEV_{i,t-1}N^-_{i,t}+u_1LEV_{i,t-1}+u_2LEV_{i,t-1}N^-_{i,t}+w_{i,t}\\
\frac{g_{i,t}}{1+g_{i,t}}=z+z_2N^-_{i,t}+\delta_1 LEV_{i,t-1}+\delta_2 LEV_{i,t-1}N^-_{i,t}+s_1LEV_{i,t-1}+s_2LEV_{i,t-1}N^-_{i,t}+z_{i,t}
\end{cases}
$$
4. 对不同特征企业（国企与民企，杠杆过高与杠杆偏低）做分组回归，比较β、修正后市值 SOA 及偏误

**遇到的问题：** 市值负债率这个指标在中国的公司研究中很少被赋予参考意义，大多数研究以及现实政策直接只关注账面的资产负债率，故感觉中国的市值 SOA 存在估算偏误这一发现的现实意义/贡献较难落脚。
