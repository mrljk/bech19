
### 黄蔓琪 的项目主页


---

### 推文写作格式要求

> **特别注意：** 除非万不得已，尽量不要用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。

1. **署名**： 请在推文首行填入你的真实姓名 (单位信息)，格式为 `> 张三丰 (xxx大学)，邮箱`
1. **标题和层级：** 推文的各个小节需要顺序编号，一级标题用 **「## 1. 一级标题」** (说明：`##` 后要添加一个空格)，二级标题用 **「### 1.1 二级标题」**，最多用到三级标题，即「#### 1.1.1 三级标题」。如果后续还需细分，可以用 **条目(item)** 或独占一行的粗体文字代替。这主要是因为，多数 Markdown 编辑器的一级标题字号都太大了。
1. **段落：** 段落之间要空一行，这样网页上的显示效果比较好；每个段落不要太长 (最好不好超过 200 字，4 行以内比较好)，否则网页或手机阅读时会比较累；
1. **中英文混排**：英文字符和数字两侧要空一格，否则中英文混排时字符间距过小；例如：「**我用Stata已经15年了(Cox,2019)，但Arlion(2018)说他刚用了14年。**」要修改为：「**我用 Stata 已经 15 年了 (Cox, 2019)，但 Arlion (2018) 说他刚用了 14 年。**」。注意，文中所有圆括号都要在半角模式下输入，`(` 左侧和 `)` 右侧各添加一个空格。
1. **Stata 相关**
  - Stata 要写为 「Stata」(首字母大写，无需加粗)，不要写成 「stata」或「STATA」。
  - 变量名用粗体 (如 \*\*varname\*\* &rarr; **varname**)；
  - Stata 命令用高亮显示 (如 \`regress\` &rarr; `regress`)；
  - 多行 Stata 命令和结果展示使用 **代码块样式**，即使用首尾 **\`\`\`** 包围，首行为 「\`\`\`stata」，尾行为「\`\`\`」。
  - **特别注意：** 除非万不得已，尽量不要用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。
5. **数学公式：** (1) 单行公式：用 **\$\$**math**\$\$** 包围的单行数学公式上下各空一行，以保证公式和正文之间的段落间距合理。(2) 行内公式：可以使用 **\$**math**\$** 包围。为了保证在知乎，简书和码云中都能正常显示公式，请把 $y=x\beta$ 写成 `$y=x\beta$`，而不要写成 `$ y=x\beta $` (内侧多加了两个空格)。&emsp; **惊喜：** 无论是网页还是 PDF 中的数学公式，都可以使用 mathpix (https://mathpix.com/) 软件扫描后识别成 LaTeX 数学公式，非常快捷。参见 [神器-数学公式识别工具 mathpix](https://www.jianshu.com/p/1f0506163694)
6. **图片**： 参见 [如何在 Markdown 中插入图片](https://gitee.com/Stata002/StataSX2018/wikis/%E5%A6%82%E4%BD%95%E5%9C%A8%20Markdown%20%E4%B8%AD%E6%B7%BB%E5%8A%A0%E5%9B%BE%E7%89%87?sort_id=1422837)
1. **版权和引用：** 推文若为英文翻译稿，请务必在文首注明来源(独占一行)。Markdown 格式为：「\**Source：**\[原英文标题](网址)」，显示效果为「**Source：**[原英文标题](网址)」。文中若有从别处复制粘贴而来的内容，要标明出处。
1. **底部文本：** 推文底部的文字介绍无需添加，在修改完后会由总编统一添加。



