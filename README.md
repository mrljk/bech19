
&emsp;


详情请进入 [【Wikis】](https://gitee.com/arlionn/bech19/wikis/Home) 查看。

### $\color{red}{News}$

> 加入平台：
1. 申请一个码云账号 (https://gitee.com);
2. 点击 [[邀请码]](https://gitee.com/arlionn/bech19/invite_link?invite=99d36473e26408ba21ff7ba2c68ed4e613f6202527fa71b7b7993663152babc53bb088706166be5a828b69667c84aeba)，获取编辑权限。

&emsp;

&emsp;

#### 项目介绍
2019本科毕业论文

### 写作指南
1. 请认真阅读 [【Wikis】](https://gitee.com/arlionn/bech19/wikis/Home)，里面详述了论文各个阶段的要求和注意事项，也提供了一些参考资料。
1. 日后论文写作过程中各个版本的文件都统一放在码云账号下，完成后微信通知我即可。若你觉得有必要，可以同时发送邮件给我。
1. 每人一个文件夹，请将各个阶段的资料上传至自己名下。
1. 我会在你提交文件后在线给出评论，也都在码云系统中进行，以便于其他组员可以分享大家都面临的问题，如论文格式等。
1. 写论文过程中请整理好你的文件，论文完成后要把所有用于重现论文的资料打包发送给我备份。建议文件夹设定方式为：【姓名】

```
*-定义论文写作文件夹 (请将如下命令贴入你的 Stata dofile 中，一次性执行)

. global name "连玉君"  //填入你的姓名
. cd D:\  // 可以更改为其他路径
. cap mkdir $name
. cd $name
. mkdir Data     //存放数据
. mkdir Refs     //存放参考文献
. mkdir docs     //存放论文各个阶段的文档
. mkdir dofiles  //论文各章或各个阶段的dofiles
. mkdir adofiles //存放外部或自编命令 (若有)
* ftree, save(Lian) d(tree)
```
最终的文件结构如下：
```
D:\连玉君/.
│  _Main_Lian.do
│  
├─adofiles
├─Data
├─docs
├─dofiles
└─Refs
```
