#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 18:08:31 2020

@author: hujie
"""

from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfinterp import process_pdf
from io import StringIO
import os

os.chdir("/Users/hujie/Desktop/test")
path = os.getcwd()
file_name_list = os.listdir(path)

   
def convert_pdf_to_txt(path):
    with open(path,'rb') as f:
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, laparams=laparams)
        
        process_pdf(rsrcmgr, device, f)
        text = retstr.getvalue()
        device.close()
        retstr.close()
        return text


for each in file_name_list:
    if each.endswith("pdf"):
        fn = each.rstrip("pdf")+"txt"   
        temp = open(fn,"w")
        text = convert_pdf_to_txt(each)
        temp.write(text)
        temp.close()
